
describe('Command', () => {
  it('kowns if is move forward', () => {

    const command = new Command('M')

    expect(command.isMoveForward()).toEqual(true)
    expect(command.isTurnRigth()).toEqual(false)
    expect(command.isTurnLeft()).toEqual(false)
  })

  it('kowns if is turn rigth', () => {

    const command = new Command('R')

    expect(command.isTurnRigth()).toEqual(true)
    expect(command.isMoveForward()).toEqual(false)
    expect(command.isTurnLeft()).toEqual(false)
  })

  it('kowns if is turn left', () => {

    const command = new Command('L')

    expect(command.isTurnLeft()).toEqual(true)
    expect(command.isMoveForward()).toEqual(false)
    expect(command.isTurnRigth()).toEqual(false)
  })
})