
describe('Commands', () => {
  it('iterates commands', () => {
    let timesIterated = 0
    const commands = new Commands('LL')

    commands.forEach((_) => {
      timesIterated += 1
    })

    expect(timesIterated).toEqual(2)
  })

  it('retrieves a command for every iteration', () => {
    const result = []
    const commands = new Commands('RLM')

    commands.forEach((command) => {
      result.push(command.isTurnLeft())
    })

    expect(result).toEqual([false, true, false])
  })
})
