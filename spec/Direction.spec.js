
describe('Direction', () => {
  it('turns left', () => {
    const direction = new Direction('N')

    direction.turnLeft()

    expect(direction.current()).toEqual('W')
  })

  it('turns twice to left', () => {
    const direction = new Direction('N')

    direction.turnLeft()
    direction.turnLeft()

    expect(direction.current()).toEqual('S')
  })

  it('turns tree times to left', () => {
    const direction = new Direction('N')

    direction.turnLeft()
    direction.turnLeft()
    direction.turnLeft()

    expect(direction.current()).toEqual('E')
  })

  it('turns four times to left', () => {
    const direction = new Direction('N')

    direction.turnLeft()
    direction.turnLeft()
    direction.turnLeft()
    direction.turnLeft()

    expect(direction.current()).toEqual('N')
  })

  it('turns rigth', () => {
    const direction = new Direction('N')

    direction.turnRigth()

    expect(direction.current()).toEqual('E')
  })

  it('turns twice to rigth', () => {
    const direction = new Direction('N')

    direction.turnRigth()
    direction.turnRigth()

    expect(direction.current()).toEqual('S')
  })

  it('turns tree times to rigth', () => {
    const direction = new Direction('N')

    direction.turnRigth()
    direction.turnRigth()
    direction.turnRigth()

    expect(direction.current()).toEqual('W')
  })

  it('turns four times to rigth', () => {
    const direction = new Direction('N')

    direction.turnRigth()
    direction.turnRigth()
    direction.turnRigth()
    direction.turnRigth()

    expect(direction.current()).toEqual('N')
  })
})