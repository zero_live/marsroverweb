
describe('MarsRover', () => {
  const samplePosition = { x: 1, y: 1, d: 'N' }

  it('lands in a position with a direcction', () => {
    const landingPosition = samplePosition

    const rover = Rover.toLand(landingPosition)

    expect(rover.currentPosition()).toEqual(landingPosition)
  })

  it('does nothing if retrieves empty commands', () => {
    const landingPosition = samplePosition
    const emptyCommand = ''
    const rover = Rover.toLand(landingPosition)

    rover.execute(emptyCommand)

    expect(rover.currentPosition()).toEqual(landingPosition)
  })

  it('moves forward', () => {
    const moveForwardCommand = 'M'
    const finalPosition = { ...samplePosition, ...{ y: 2 } }
    const rover = Rover.toLand(samplePosition)

    rover.execute(moveForwardCommand)

    expect(rover.currentPosition()).toEqual(finalPosition)
  })

  it('turns rigth', () => {
    const turnRigthCommand = 'R'
    const finalPosition = { ...samplePosition, ...{ d: 'E' } }
    const rover = Rover.toLand(samplePosition)

    rover.execute(turnRigthCommand)

    expect(rover.currentPosition()).toEqual(finalPosition)
  })

  it('turns left', () => {
    const turnLeftCommand = 'L'
    const finalPosition = { ...samplePosition, ...{ d: 'W' } }
    const rover = Rover.toLand(samplePosition)

    rover.execute(turnLeftCommand)

    expect(rover.currentPosition()).toEqual(finalPosition)
  })

  it('executes some commands at once', () => {
    const finalPosition = { ...samplePosition, ...{ d: 'W', x: 0 } }
    const rover = Rover.toLand(samplePosition)

    rover.execute('LM')

    expect(rover.currentPosition()).toEqual(finalPosition)
  })
})
