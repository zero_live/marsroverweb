
describe('Position', () => {
  const samplePosition = { x: 1, y: 1, d: 'N' }

  it('turns left', () => {
    const finalPosition = { ...samplePosition, ...{ d: 'W' }}
    const position = new Position(samplePosition)

    position.turnLeft()

    expect(position.current()).toEqual(finalPosition)
  })

  it('turns rigth', () => {
    const finalPosition = { ...samplePosition, ...{ d: 'E' }}
    const position = new Position(samplePosition)

    position.turnRigth()

    expect(position.current()).toEqual(finalPosition)
  })

  it('moves forward to north', () => {
    const finalPosition = { ...samplePosition, ...{ y: 2 }}
    const position = new Position(samplePosition)

    position.moveForward()

    expect(position.current()).toEqual(finalPosition)
  })

  it('moves forward to west', () => {
    const position = new Position(samplePosition)

    position.turnLeft()
    position.moveForward()

    expect(position.current()).toEqual({ x: 0, y: 1, d: 'W'})
  })

  it('moves forward to east', () => {
    const position = new Position(samplePosition)

    position.turnRigth()
    position.moveForward()

    expect(position.current()).toEqual({ x: 2, y: 1, d: 'E'})
  })

  it('moves forward to south', () => {
    const position = new Position(samplePosition)

    position.turnRigth()
    position.turnRigth()
    position.moveForward()

    expect(position.current()).toEqual({ x: 1, y: 0, d: 'S'})
  })
})