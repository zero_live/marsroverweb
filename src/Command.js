
class Command {
  constructor(raw) {
    this.command = raw
  }

  isMoveForward() {
    return this.command === 'M'
  }

  isTurnRigth() {
    return this.command === 'R'
  }

  isTurnLeft() {
    return this.command === 'L'
  }
}