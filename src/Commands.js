
class Commands {
  constructor(rawCommands) {
    this.commands = this._toCommands(rawCommands)
  }

  forEach(callback) {
    this.commands.forEach((command) => {
      callback(command)
    })
  }

  _toCommands(rawCommands) {
    const letters = rawCommands.split('')

    const commands = letters.map(letter => new Command(letter))

    return commands
  }
}
