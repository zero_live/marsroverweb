
class Coordenates {
  constructor(x, y) {
    this.x = x
    this.y = y
  }

  moveLeft() {
    this.x -= 10
  }

  moveRigth() {
    this.x += 10
  }

  moveTop() {
    this.y += 10
  }

  moveBottom() {
    this.y -= 10
  }

  toDict() {
    return {
      x: this.x,
      y: this.y
    }
  }
}
