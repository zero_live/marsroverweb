
class Direction {
  WEST_LETTER() { return 'W' }
  NORTH_LETTER() { return 'N' }
  EAST_LETTER() { return 'E' }
  SOUTH_LETTER() { return 'S' }
  DIRECTIONS() { return [this.SOUTH_LETTER(), this.WEST_LETTER(), this.NORTH_LETTER(), this.EAST_LETTER()] }
  FIRST_DIRECTION() { return 0 }
  LAST_DIRECTION() { return this.DIRECTIONS().length - 1 }

  constructor(direction) {
    this.direction = this._translate(direction)
  }

  turnRigth() {
    this.direction += 1

    if (this._isOutOfDirections()) { this._turnToSouth() }
  }

  turnLeft() {
    this.direction -= 1

    if (this._isOutOfDirections()) { this._turnToEast() }
  }

  current() {
    return this._toString()
  }

  isWest() {
    return this._toString() === this.WEST_LETTER()
  }

  isNorth() {
    return this._toString() === this.NORTH_LETTER()
  }

  isEast() {
    return this._toString() === this.EAST_LETTER()
  }

  isSouth() {
    return this._toString() === this.SOUTH_LETTER()
  }

  _turnToSouth() {
    this.direction = this.FIRST_DIRECTION()
  }

  _turnToEast() {
    this.direction = this.LAST_DIRECTION()
  }

  _toString() {
    return this.DIRECTIONS()[this.direction]
  }

  _translate(direction) {
    return this.DIRECTIONS().indexOf(direction)
  }

  _isOutOfDirections() {
    return (this.direction < this.FIRST_DIRECTION() || this.direction > this.LAST_DIRECTION())
  }
}
