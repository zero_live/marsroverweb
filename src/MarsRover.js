
class Rover {
  static toLand(rawPosition) {
    const position = new Position(rawPosition)

    return new Rover(position)
  }

  constructor(position) {
    this.position = position
  }

  execute(rawCommands) {
    const commands = new Commands(rawCommands)

    commands.forEach((command) => {
      if (command.isMoveForward()) { this.position.moveForward() }
      if (command.isTurnRigth()) { this.position.turnRigth() }
      if (command.isTurnLeft()) { this.position.turnLeft() }
    })
  }

  currentPosition() {
    return this.position.current()
  }
}
