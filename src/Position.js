
class Position {
  constructor({ x, y, d }) {
    this.coordenates = new Coordenates(x, y)
    this.direction = new Direction(d)
  }

  moveForward() {
    if (this.direction.isWest()) { this.coordenates.moveLeft() }
    if (this.direction.isNorth()) { this.coordenates.moveTop() }
    if (this.direction.isEast()) { this.coordenates.moveRigth() }
    if (this.direction.isSouth()) { this.coordenates.moveBottom() }
  }

  turnRigth() {
    this.direction.turnRigth()
  }

  turnLeft() {
    this.direction.turnLeft()
  }

  current() {
    return {
      ...this.coordenates.toDict(),
      ...{ d: this.direction.current() },
    }
  }
}
